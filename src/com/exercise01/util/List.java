package com.exercise01.util;

import java.util.function.Consumer;
import java.util.function.Function;

public class List<T extends Object> {

    private class Node {

        T obj;
        Node next;

        Node(T obj) {
         this.obj = obj;
        }
    }

    private final Consumer<Node> printNode = (node) -> System.out.println(node.obj.toString());

    private Node node;

    private Node getNode() {
        return node;
    }

    private Node filterStatement(Node node, Function<T, Boolean> lambda) {
        if(null == node || null == lambda) {
            return null;
        }

        boolean found = lambda.apply(node.obj);
        if(found) {
            return node;
        }
        return  filterStatement(node.next, lambda);
    }

    private Node revertList(Node currentNode) {

        if(null != currentNode.next) {
            Node next = currentNode.next;
            revertList(next).next = currentNode;
            currentNode.next = null;
        } else {
            node = currentNode;
        }

        return currentNode;
    }

    private Node executeStatement(Node node, Consumer<Node> lambda) {

        if(null != lambda) {
            lambda.accept(node);
        }

        if(null != node.next) {
            executeStatement(node.next, lambda);
        }

        return node;
    }

    private Node getNext(Node node) {
        return null != node.next ? getNext(node.next) : node;
    }

    public void add(T newObj) {
        if(null == node ) {
            node = new Node(newObj);
        } else {
            getNext(node).next = new Node(newObj);
        }
    }

    public void addAll(List list) {
        Node lastNode = getNext(node);
        lastNode.next = list.getNode();
    }

    public T findBy(Function lambda) {
        return (T) filterStatement(node,lambda).obj;
    }


    public void revertList() {
        revertList(node);
    }

    public void printAll() {
        executeStatement(node, printNode );
    }
}
