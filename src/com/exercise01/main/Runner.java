package com.exercise01.main;

import com.exercise01.model.Order;
import com.exercise01.util.List;

public class Runner {


    public static void main(String[] args)
    {
        List<Order> orders = new List<>();
        orders.add(new Order(3534L, "gdgdgsd"));
        orders.add(new Order(3435L, "fdffsss"));
        orders.add(new Order(3541L, "rcedsff"));
        System.out.println("Initial list");
        orders.printAll();


        List<Order> newOrders = new List<>();
        newOrders.add(new Order(1111L, "fdfghed"));
        newOrders.add(new Order(1555L, "lkgfjdw"));
        orders.addAll(newOrders);
        System.out.println("merge lists");
        orders.printAll();

        Order foundOrder = orders.findBy( x -> ((Order) x).getId() == 3435L );
        System.out.print("found order: ");
        System.out.println(foundOrder);


        System.out.println("reversed list");
        orders.revertList();
        orders.printAll();
    }
}
